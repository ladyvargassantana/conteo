<?php

namespace app\controllers;

use app\models\Candidatos;
use app\models\Mesas;
use app\models\Rol;
use app\models\Votos;
use app\searchs\CandidatoSearch;
use app\searchs\VotoSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [ //aquí se ponen todas las acciones que requieren login obligatorio
                    'index',
                    'logout',
                    'votos',
                    'nuevos-votos',
                    'candidatos',
                    'nuevos-candidatos',
                    'candidatos_user',
                ],
                'rules' => [//aquí se separa esas mismas acciones por roles
                    [
                        'actions' => [
                            'index',
                            'logout'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => [
                            'candidatos',
                            'nuevos-candidatos',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Rol::getAdmin();
                        },
                    ],
                    [
                        'actions' => [
                            'votos',
                            'nuevos-votos',
                            'candidatos_user',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Rol::getUser();
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionVotos()
    {
        $searchModel = new VotoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('votos', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionNuevosVotos()
    {
        $model = new Votos();
        if ($model->load(Yii::$app->request->post())) {
            $idMesa = Mesas::porUsuario();//se lo pasa como parámetro a esa función
            if ($idMesa)
                $model->id_mesa = $idMesa['id'];
            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect('votos');
                }
            }
        }
        return $this->render('nuevos_votos', ['model' => $model]);
    }

    public function actionNuevosCandidatos()
    {
        $model = new Candidatos();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect('candidatos');
            }
        }
        return $this->render('nuevos_candidatos', ['model' => $model]);
    }

    public function actionCandidatos()
    {
        $searchModel = new CandidatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('candidatos', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    public function actionCandidatos2()
    {
        $searchModel = new CandidatoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('candidatos_user', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }


    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
