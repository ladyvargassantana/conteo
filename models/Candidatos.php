<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property string $nombre
 * @property string $apellido
 * @property int $id_cargo
 * @property int $id_partido
 */
class Candidatos extends ActiveRecord
{
    public $votos;

    public static function tableName()
    {
        return 'candidatos';
    }

    public function rules()
    {
        return [
            [
                [
                    'nombre',
                    'apellido',
                    'id_cargo',
                    'id_partido',
                ], 'required'], //La regla REQUIRED obliga a llenar el formulario de NUEVO ARTÍCULO
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'nombre' => 'Nombre',
            'apellido' => 'Apellidos',
            'id_cargo' => 'Cargo',
            'id_partido' => 'Partido',
        ];
    }

    public static function obtenerCandidatos()
    {
        return self::find()->asArray()->all();
    }

    public static function obtenerUnCandidatoPorId($id)
    {
        return self::find()->where(['id' => $id])->asArray()->one();
    }
}