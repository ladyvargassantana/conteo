<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property string $Cargo

 */
class Cargo extends ActiveRecord
{
    public static function tableName()
    {
        return 'cargo';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'Cargo', //todas esas mayúsculas le van a hacer perder tiempo buscando errores
                ], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }

    public static function obtenerCargos()
    {
        return self::find()->asArray()->all();
    }
}