<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property int $No_mesa
 * @property string $tipo_mesa
 * @property int $id_user
 * @property int $id_recinto
 */
class Mesas extends ActiveRecord
{
    public static function tableName()
    {
        return 'mesa';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'No_mesa',
                    'tipo_mesa',
                    'id_user',
                    'id_recinto',
                ], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }

    public static function porUsuario()
    {
        return self::find()->where(['id_user' => Yii::$app->user->identity->id])->asArray()->one();
    }
}