<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property string $nombre
 * @property string $No_lista

 */
class Partido extends ActiveRecord
{
    public static function tableName()
    {
        return 'partido_politico';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'nombre',
                    'No_lista',
                ], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
        ];
    }

    public static function obtenerPartidos()
    {
        return self::find()->asArray()->all();
    }
}