<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property string $nombre
 */
class Recinto extends ActiveRecord
{
    public static function tableName()
    {
        return 'recintos';
    }

    public function rules()
    {
        return [
            [
                [
                    'id',
                    'nombre',
                ], 'required'],
        ];
    }

    public static function obtenerPorId($id)
    {
        return self::find()->where(['id' => $id])->asArray()->one();
    }

}