<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * User model
 * @property int $id
 * @property string $numero
 * @property int $id_candidato
 * @property int $id_mesa
 */
class Votos extends ActiveRecord
{
    public static function tableName()
    {
        return 'votos';
    }

    public function rules()
    {
        return [
            [
                [
                    'numero',
                    'id_candidato',
                    'id_mesa',
                ], 'required'], //el Validate hace que funcionen las reglas, en este caso que los atributos de aquí sean REQUERIDOSl
            [
                [
                    'numero',
                ], 'number'],
            [
                [
                    'numero',
                ], 'compare', 'compareValue' => 0, 'operator' => '>'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'numero' => 'Número de votos',
            'id_candidato' => 'Candidato',
        ];
    }
}