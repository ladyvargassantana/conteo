<?php

namespace app\searchs;

use app\models\Candidatos;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CandidatoSearch extends Candidatos
{
    /*
     * Esta clase extiende del modelo mensionado y sirve para realizar las búsquedas dinámicas
     * dentro del GridView de la Vista ARTÍCULOS
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'nombre',
                    'apellido',
                ], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Candidatos::find()
            ->select('candidatos.*, sum(numero) votos')
            ->leftJoin('votos', 'candidatos.id = votos.id_candidato')
            ->groupBy('candidatos.id')
            ->orderBy(['votos' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*
         * Son los parámetros por el cual se puede realizar la búsqueda dentro del gridview
         */
        $query->andFilterWhere(['like', 'id', $this->id]);
        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['like', 'apellido', $this->apellido]);

        return $dataProvider;
    }

}
