<?php

namespace app\searchs;

use app\models\Mesas;
use app\models\Votos;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class VotoSearch extends Votos
{
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'numero'
                ], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $mesa = Mesas::porUsuario();
        $query = Votos::find()
            ->where(['id_mesa' => $mesa['id']])
            ->innerJoin('candidatos', 'candidatos.id=id_candidato')
            ->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /*
         * Son los parámetros por el cual se puede realizar la búsqueda dentro del gridview
         */
        $query->andFilterWhere(['or',
            ['like', 'candidatos.nombre', $this->id],
            ['like', 'candidatos.apellido', $this->id]
        ]);
        $query->andFilterWhere(['like', 'numero', $this->numero]);

        return $dataProvider;
    }

}
