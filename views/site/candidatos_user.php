<?php

use yii\grid\GridView;
use yii\helpers\Html;

?>

<div class="site-index">
    <h1>Candidatos</h1>
    <?php
    echo '<br><br>';
    echo GridView::widget([ //gridview con todos los artículos disponibles
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}\n{items}\n{pager}",
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
            ],
            'nombre',
            'apellido',
            'votos'
        ]
    ]); ?>
</div>
