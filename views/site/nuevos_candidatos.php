<?php

use app\models\Cargo;
use app\models\Partido;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo Html::a('Regresar', 'candidatos', ['class' => 'btn btn-info']);
echo '<br><br>';
//aquí va el formulario
$form = ActiveForm::begin();
echo $form->field($model, 'nombre');
echo $form->field($model, 'apellido');
echo $form->field($model, 'id_cargo')->widget(Select2::class, [//Este widget muestra en forma de DROPDOWN MENU todos los tipo de artículos disponibles
    'data' => ArrayHelper::map(Cargo::obtenerCargos(), 'id', 'Cargo'),
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => false,
        'placeholder' => '',
    ],
]);
echo $form->field($model, 'id_partido')->widget(Select2::class, [//Este widget muestra en forma de DROPDOWN MENU todos los tipo de artículos disponibles
    'data' => ArrayHelper::map(Partido::obtenerPartidos(), 'id', function ($data) {
        return $data['No_lista'] . ' ' . $data['nombre'];
    }),
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => false,
        'placeholder' => '',
    ],
]);

echo Html::submitButton('Guardar', ['class' => 'btn btn-success']);
ActiveForm::end();