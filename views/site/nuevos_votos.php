<?php

use app\models\Candidatos;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

echo Html::a('Regresar', 'votos', ['class' => 'btn btn-info']);
echo '<br><br>';
//aquí va el formulario
$form = ActiveForm::begin();
echo $form->field($model, 'id_candidato')->widget(Select2::class, [//Este widget muestra en forma de DROPDOWN MENU todos los tipo de artículos disponibles
    'data' => ArrayHelper::map(Candidatos::obtenerCandidatos(), 'id', function ($data) {
        return $data['apellido'] . ' ' . $data['nombre'];
    }),
    'pluginOptions' => [
        'allowClear' => true,
        'multiple' => false,
        'placeholder' => '',
    ],
]);
echo $form->field($model, 'numero');

echo Html::submitButton('Guardar', ['class' => 'btn btn-success']);
ActiveForm::end();