<?php

use app\models\Candidatos;
use app\models\Mesas;
use app\models\Recinto;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Mesa';

$mesa = Mesas::porUsuario();

?>

<div class="site-index">
    <table style="font-size: large;">
        <tr>
            <th>RECINTO</th>
            <td><?= ' ' . Recinto::obtenerPorId($mesa['id_recinto'])['nombre'] ?></td>
        </tr>
        <tr>
            <th>MESA</th>
            <td><?= ' ' . $mesa['No_mesa'] ?></td>
        </tr>
        <tr>
            <th>SEXO</th>
            <td><?= ' ' . $mesa['tipo_mesa'] ?></td>
        </tr>
    </table>
    <br>
    <?php
    echo Html::a('Nuevos Votos', 'nuevos-votos', ['class' => 'btn btn-success']);
    echo '<br><br>';
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{pager}\n{items}\n{pager}",
        'options' => [
            'class' => 'table-responsive',
        ],
        'columns' => [
            [
                'attribute' => 'id',
                'label' => 'Candidatos',
                'value' => function ($data) {
                    $candidato = Candidatos::obtenerUnCandidatoPorId($data['id_candidato']);
                    return $candidato['apellido'] . ' ' . $candidato['nombre'];
                }
            ],
            'numero',
        ]
    ]); ?>
</div>
